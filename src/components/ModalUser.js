import { Modal, Box, Typography, Button} from "@mui/material";
import { Label, Row, Col, Input } from "reactstrap";


const ModalUser = ({openModal, closeModalProp, style, dataClickProp}) =>{
    const onBtnClose = () =>{
        closeModalProp();
    }
    return(
    <>
        <Modal
                open={openModal}
                onClose={closeModalProp}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
            <Box sx={style}>
                <Typography mb={4} id="modal-modal-title" variant="h5" component="h2">
                    <strong>Chi tiết User </strong>
                </Typography>
                <Row>
                    <Col className="col-6">
                        <Row>
                            <Col className="col-3">
                                <Label>Firstname</Label>
                            </Col>
                            <Col className="col-9">
                                <Input value={dataClickProp.firstname} />
                            </Col>
                        </Row>
                    </Col>
                    <Col className="col-6">
                    <Row>
                            <Col className="col-3">
                                <Label>Lastname</Label>
                            </Col>
                            <Col className="col-9">
                                <Input value={dataClickProp.lastname}/>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                <Row className="mt-3">
                    <Col className="col-6">
                        <Row>
                            <Col className="col-3">
                                <Label>Country</Label>
                            </Col>
                            <Col className="col-9">
                                <Input value={dataClickProp.country}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col className="col-6">
                    <Row>
                            <Col className="col-3">
                                <Label>Subject</Label>
                            </Col>
                            <Col className="col-9">
                                <Input value={dataClickProp.subject} />
                            </Col>
                        </Row>
                    </Col>
                </Row>

                <Row className="mt-3">
                    <Col className="col-6">
                        <Row>
                            <Col className="col-3">
                                <Label>CustomerType</Label>
                            </Col>
                            <Col className="col-9">
                               <Input value={dataClickProp.customerType}  />
                            </Col>
                        </Row>
                    </Col>
                    <Col className="col-6">
                    <Row>
                            <Col className="col-3">
                                <Label>RegisterStatus</Label>
                            </Col>
                            <Col className="col-9">
                                <Input value={dataClickProp.registerStatus} />
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="mt-5">
                    <Button color="error" variant="contained" onClick={onBtnClose}>Close</Button>
                </Row>
            </Box>
         </Modal>
        
    </>
    )
}


export default ModalUser;