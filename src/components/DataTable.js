import { useEffect, useState } from "react";
import { Label } from 'reactstrap'
import {Table, TableContainer, TableHead, TableBody, TableRow, TableCell, Grid, Paper, Button, Pagination, Select, MenuItem} from '@mui/material'
import ModalUser from "./ModalUser";


const DataTable = () =>{

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 900,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        pt: 2,
        px: 4,
        pb: 3,
      };
      
      const [posts, setPost] = useState([]);
      const [openModal, setOpenModal] = useState(false);
      const [dataClick, setDataClick] = useState([]);

      const [limit, setLimit] = useState(10);
      const [page, setPage] = useState(1);
      const [noPage, setNoPage] = useState(0);

      const [refreshPage, setRefreshPage] = useState(0);
      
        const closeModal = () => setOpenModal(false);



    const getDataPosts = async (paramUrl, paramObject = {}) =>{
        const response = await fetch(paramUrl, paramObject);
        const data = await response.json();
        return data;
    }

    useEffect(()=>{
        getDataPosts("http://42.115.221.44:8080/devcamp-register-java-api/users")
        .then((data) =>{
            console.log(data);
            setNoPage(Math.ceil(data.length / limit));
            setPost(data.slice(limit * (page - 1) , limit * page));
        })
        .catch((error) =>{
            console.log(error.message);
        })
    },[page, refreshPage])

    const onBtnDetail = (element) =>{
        console.log(element);
        setOpenModal(true);
        setDataClick(element);
    }

    const onChangePage = (event, value) =>{
        setPage(value);
    }

    const onChangeLimit = (event) =>{
        setLimit(event.target.value);
        setRefreshPage(refreshPage + 1);
    }




    return(
        <div>
    <Grid container>
        <Grid item xs={2}>
            <Label><b>Select Number Page : </b></Label>
        </Grid>
        <Grid item xs={10}>
           <Select
                defaultValue={10}
                onChange={onChangeLimit}
           >
               <MenuItem value={5}>5</MenuItem>
               <MenuItem value={10}>10</MenuItem>
               <MenuItem value={25}>25</MenuItem>
               <MenuItem value={50}>50</MenuItem>

           </Select>
        </Grid>
    </Grid>
            <Grid container marginTop={3}>
                <Grid item xs={12}>
                <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center"><b>ID</b></TableCell>
            <TableCell align="center"><b>Firstname</b></TableCell>
            <TableCell align="center"><b>Lastname</b></TableCell>
            <TableCell align="center"><b>Country</b></TableCell>
            <TableCell align="center"><b>Subject</b></TableCell>
            <TableCell align="center"><b>CustomerType</b></TableCell>
            <TableCell align="center"><b>RegisterStatus</b></TableCell>
            <TableCell align="center"><b>Detail</b></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {posts.map((element, index) =>{
              return(
                  <TableRow key={index}>
                        <TableCell align="center">{element.id}</TableCell>
                        <TableCell align="center">{element.firstname}</TableCell>
                        <TableCell align="center">{element.lastname}</TableCell>
                        <TableCell align="center">{element.country}</TableCell>
                        <TableCell align="center">{element.subject}</TableCell>
                        <TableCell align="center">{element.customerType}</TableCell>
                        <TableCell align="center">{element.registerStatus}</TableCell>
                        <TableCell align="center">
                            <Button onClick={() => onBtnDetail(element)} color="success" variant="contained">Chi tiết</Button>
                        </TableCell>
                  </TableRow>
              )
          })}
                </TableBody>
            </Table>
        </TableContainer>
    </Grid>
</Grid>

<Grid marginTop={3} marginBottom={3} display="flex" justifyContent="end">
    <Pagination count={noPage} defaultPage={page} onChange={onChangePage} color="primary" />
</Grid>
            
          <ModalUser
                openModal = {openModal}
                closeModalProp = {closeModal}
                style = {style}
                dataClickProp = {dataClick}
          />
        </div>
    )
}

export default DataTable;