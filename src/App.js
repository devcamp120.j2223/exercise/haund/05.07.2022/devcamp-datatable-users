import DataTable from "./components/DataTable";
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from "reactstrap";

function App() {
  return (
    <Container>
        <h2 className="text-center text-info">DANH SÁCH ĐĂNG KÝ</h2>
        <DataTable/>
    </Container>
  );
}

export default App;
